package edu.ntnu.stud;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StudentTest {
    @Nested
    @DisplayName("Positive tests for the Student class")
    public class PositiveStudentTests {

        @Test
        @DisplayName("Student constructor does not throw exp on valid name")
        public void studentConstructorDoesNotThrowException() {
            try {
                Student student1 = new Student("Magnus");
                assertNotNull(student1);
            } catch (IllegalArgumentException e) {
                fail("The test failed, because the constructor did throw the exception: " + e.getMessage());
            }
        }

    }

    @Nested
    @DisplayName("Negative tests for the Student class")
    public class NegativeStudentTests {
        @Test
        @DisplayName("Student constructor does throw exp on invalid name")
        public void studentConstructorDoesThrowException() {
            try {
                Student student1 = new Student("");
                fail("The test failed, because the constructor did not throw the exception");
            } catch (IllegalArgumentException e) {
                assertEquals("The name was blank, please try again.", e.getMessage());
            }
        }
    }
}
