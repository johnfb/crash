package edu.ntnu.stud;

import java.util.Scanner;

/**
 * This is the main class for the train dispatch application.
 */
public class App {
    /**
     * The main method for the train dispatch application.
     * More documentation
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        TaskOverview to = new TaskOverview();
        Scanner in = new Scanner(System.in);
        boolean run = true;
        int choice;
        while (run) {
            System.out.println("Menu\n" +
                    "------------------------------------------\n" +
                    "1: Print student count.\n" +
                    "2: Print a students completed tasks count\n" +
                    "3: Register a new student\n" +
                    "4: Increase a students completed tasks\n" +
                    "5: Print all\n" +
                    "0: Exit\n" +
                    "------------------------------------------");
            choice = Integer.parseInt(in.nextLine());
            switch (choice) {
                case 1:
                    System.out.println("There are " + to.getNumStudents() + " students registered.");
                    break;
                case 2:
                    System.out.println("Enter student name: ");
                    String name = in.nextLine();
                    try {
                        int completedTasks = to.getCompletedTasks(name);
                        if (completedTasks == -1) {
                            System.out.println("Student " + name + " not found.");
                        } else {
                            System.out.println("Student " + name + " have completed " + completedTasks + " tasks");
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("Enter a valid name.");
                    }
                    break;
                case 3:
                    System.out.println("Enter the new students name: ");
                    name = in.nextLine();

                    try {
                        boolean success = to.newStudent(name);
                        if (success) {
                            System.out.println("New student registered.");
                        } else {
                            System.out.println("Could not register new student. List full.");
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("Enter a valid name.");
                    }

                    break;
                case 4:
                    System.out.println("Enter student name: ");
                    name = in.nextLine();
                    System.out.println("Enter increase: ");
                    int increase = Integer.parseInt(in.nextLine());
                    try {
                        boolean success = to.increaseCompletedTasks(name, increase);
                        if (success) {
                            System.out.println("Completed tasks registered for student: " + name + ".");
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("Enter a valid name.");
                    }
                    break;
                case 5:
                    System.out.println(to);
                    break;
                case 0:
                    System.out.println("Shutting down...");
                    run = false;
                    break;
                default:

            }


        }


    }
}
