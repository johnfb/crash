package edu.ntnu.stud;

import java.util.ArrayList;

public class TaskOverview {
    private ArrayList<Student> students;

    public TaskOverview() {
        students = new ArrayList<>();
    }

    int getNumStudents() {
        return students.size();
    }

    int getCompletedTasks (String name) {
        Student tempStudent = new Student(name);
        if(!students.contains(tempStudent)) return -1;
        for (Student student : students) {
            if(student.equals(tempStudent)) return student.getCompletedTasks();
        }
        return -1;

    }

    public boolean newStudent(String name) {
        Student tempStudent = new Student(name);
        if (students.contains(tempStudent)) return false;
        students.add(tempStudent);
        return true;
    }

    public boolean increaseCompletedTasks(String name, int increase) {
        Student tempStudent = new Student(name);
        if(!students.contains(tempStudent)) return false;
        for(Student student: students) {
            if (student.equals(tempStudent)) {
                student.increaseTaskAmount(increase);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
          StringBuilder sb = new StringBuilder();

          for(Student student: students) {
              sb.append(student).append("\n");
          }
          sb.append("Total student: " + getNumStudents());

          return sb.toString();
    }
}
