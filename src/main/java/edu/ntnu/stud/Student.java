package edu.ntnu.stud;

import java.util.Objects;

public class Student {
    private final String name;
    private int completedTasks;

    public Student(String name) {
        if(name.equals("")) throw new IllegalArgumentException("The name was blank, please try again.");
        this.name = name;
        this.completedTasks = 0;
    }

    public String getName() {
        return name;
    }

    public int getCompletedTasks() {
        return completedTasks;
    }

    public void setCompletedTasks(int completedTasks) {
        this.completedTasks = completedTasks;
    }

    @Override
    public String toString() {
        return "Name: " + name + '\'' + "Completed tasks: " + completedTasks;
    }

    public void increaseTaskAmount(int increase) {
        completedTasks += increase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name);
    }

}
